import XCTest

import dBridgesTests

var tests = [XCTestCaseEntry]()
tests += dBridgesTests.allTests()
XCTMain(tests)
