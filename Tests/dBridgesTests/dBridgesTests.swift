import XCTest
@testable import dBridges

final class dBridgesTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        //XCTAssertEqual(dBridges().connect(), "Hello, World!")
        let dbridge = dBridges()
        dbridge.auth_url =  "https://eu-an01.cloudbridges.io:16446/client/v1/authenticate"
        dbridge.appkey = "f293f87e969b092f7ab60cb7d5715f437846dc9c0ef464c3c04c972765c8ef31.i7d.bfb88cef48a64c4faad4492da52a4cfc"
        dbridge.autoReconnect = false
        
        try? dbridge.connectionstate?.bind("connect_error", {  (event: Any) in
            print(event)
        })
        
        
        try? dbridge.connectionstate?.bind("connected", {  (event: Any) in
            print(event)
        })
        
        
        try? dbridge.connectionstate?.bind("disconnected", {  (event: Any) in
            print(event)
        })
        
        
        try? dbridge.connectionstate?.bind("connecting", {  (event: Any) in
            print(event)
        })
      
        try? dbridge.connectionstate?.bind("state_change", {  (event: Any) in
            print(event)
        })
        
        try? dbridge.connectionstate?.bind("connection_break", {  (event: Any) in
            print(event)
        })
        
        
        try? dbridge.connect()
        
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
