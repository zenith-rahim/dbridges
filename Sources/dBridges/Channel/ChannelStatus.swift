//
//  File.swift
//  
//
//  Created by mac on 04/04/22.
//

import Foundation

enum ChannelStatus
{
    static let SUBSCRIPTION_INITIATED = "subscription_initiated"
   static let SUBSCRIPTION_PENDING = "subscription_pending"
   static let SUBSCRIPTION_ACCEPTED = "subscription_accepted"
   static let SUBSCRIPTION_ERROR = "subscription_error"
   static let CONNECTION_INITIATED = "connection_initiated"
   static let CONNECTION_PENDING = "connection_pending"
   static let CONNECTION_ACCEPTED = "connection_accepted"
   static let CONNECTION_ERROR = "connection_error"
   static let UNSUBSCRIBE_INITIATED = "unsubscribe_initiated"
   static let UNSUBSCRIBE_ACCEPTED = "unsubscribe_accepted"
   static let UNSUBSCRIBE_ERROR = "unsubscribe_error"
   static let DISCONNECT_INITIATED = "disconnect_initiated"
   static let DISCONNECT_ACCEPTED = "disconnect_accepted"
   static let DISCONNECT_ERROR = "disconnect_error"
}
