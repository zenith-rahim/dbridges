//
//  File.swift
//  
//
//  Created by mac on 04/04/22.
//

import Foundation

 public class Connect {

    var  _channelName:String
    var _sid:String
    var  _dbcore:dBridges
    var _isOnline: Bool;
    var _dispatch: EventDispatcher;
    let ccSupportedEvents:[String] = [
        "dbridges:channel.removed",
         "dbridges:connect.success",
         "dbridges:connect.fail",
         "dbridges:disconnect.success",
         "dbridges:disconnect.fail",
         "dbridges:reconnect.success",
         "dbridges:reconnect.fail",
         "dbridges:participant.joined",
         "dbridges:participant.left"
    ]


    init(_ channelName:String, _ sid: String , _ dBCoreObject: dBridges)
    {
        self._channelName = channelName;
        self._sid = sid;
        self._dbcore = dBCoreObject;
        self._isOnline = false;
        self._dispatch =  EventDispatcher();
    }

   public func getChannelName()->String{
        return self._channelName;
    }

    public func  isOnline()->Bool {
        return self._isOnline;
    }

    public func set_isOnline(_ value:Bool) {
        self._isOnline = value;
    }



    public func bind(_ eventName: String, _ eventhandler: @escaping  dBConnectCallBack ) throws {
        if eventName.isEmpty {
            throw dBError("103")
        }
        
        if !ccSupportedEvents.contains(eventName) {
            throw dBError("103")
        }
        
        try self._dispatch.bind(eventName, eventhandler);
        
    }


    public func unbind(_ eventName: String) throws {
        if eventName.isEmpty {
            throw dBError("103")
        }
        
        if !ccSupportedEvents.contains(eventName) {
            throw dBError("103")
        }
        self._dispatch.unbind(eventName)
    }
    
    
    
    public func unbind(){
        self._dispatch.unbind();
    }

    
    func emit_channelStatus(_ eventName:String, _ EventInfo:Any , _ matadatav:MetaData) {
        self._dispatch.emit_channelStatus(eventName, EventInfo, matadatav);
    }
    


   public func publish(_ eventName: String  , _ eventData: String,  seqnum: Int64) throws  {
        if !self._isOnline { throw dBError("E014" ) }
        
        if self._channelName.lowercased() == "sys:*" { throw  dBError("E015") }
        
        if eventName.isEmpty { throw dBError("E058") }
        
        let m_status:Bool = Util.updatedBNewtworkSC(self._dbcore, MessageType.PUBLISH_TO_CHANNEL, self._channelName, "", eventData, eventName, "", 0, seqnum);
        if !m_status {throw dBError("E014") }
        return;
    }

   public func publish(_ eventName: String  ,_ eventData:String) throws {
    if !self._isOnline { throw dBError("E014" ) }
    
    if self._channelName.lowercased() == "sys:*" { throw  dBError("E015") }
    
    if eventName.isEmpty { throw dBError("E058") }
    
        let m_status:Bool = Util.updatedBNewtworkSC(self._dbcore, MessageType.PUBLISH_TO_CHANNEL, self._channelName, "", eventData, eventName, "", 0, 0);
    
    if !m_status {throw dBError("E014") }
        return;
    }


}
