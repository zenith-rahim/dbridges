//
//  File.swift
//  
//
//  Created by mac on 04/04/22.
//

import Foundation

public struct ChannelInfo {

    let name: String
    let type: String
    let isonline: Bool

}
