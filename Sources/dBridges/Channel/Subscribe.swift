//
//  File.swift
//  
//
//  Created by mac on 04/04/22.
//

import Foundation


public class Subscribe: EventDispatcher {
    var  _channelName:String
    var _sid:String
    var  _dbcore:dBridges
    var _isOnline: Bool;
   // var _dispatch: dispatcher;

    init(_ channelName:String, _ sid: String , _ dBCoreObject: dBridges)
    {
        self._channelName = channelName;
        self._sid = sid;
        self._dbcore = dBCoreObject;
        self._isOnline = false;
       // self._dispatch = dispatcher();
    }

    public func getChannelName()->String{
        return self._channelName;
    }

    public func  isOnline()->Bool {
        return self._isOnline;
    }

    public func set_isOnline(_ value:Bool) {
        self._isOnline = value;
    }


    public func publish(_ eventName: String  , _ eventData: String,  seqnum: Int64) throws  {
        if !self._isOnline { throw dBError("E014" ) }
        
        if self._channelName.lowercased() == "sys:*" { throw  dBError("E015") }
        
        if eventName.isEmpty { throw dBError("E058") }
        
        let m_status:Bool = Util.updatedBNewtworkSC(self._dbcore, MessageType.PUBLISH_TO_CHANNEL, self._channelName, "", eventData, eventName, "", 0, seqnum);
        if !m_status {throw dBError("E014") }
        return;
    }

    public func publish(_ eventName: String  ,_ eventData:String) throws {
    if !self._isOnline { throw dBError("E014" ) }
    
    if self._channelName.lowercased() == "sys:*" { throw  dBError("E015") }
    
    if eventName.isEmpty { throw dBError("E058") }
    
        let m_status:Bool = Util.updatedBNewtworkSC(self._dbcore, MessageType.PUBLISH_TO_CHANNEL, self._channelName, "", eventData, eventName, "", 0, 0);
    
    if !m_status {throw dBError("E014") }
        return;
    }


}
