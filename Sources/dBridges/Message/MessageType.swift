//
//  File.swift
//  
//
//  Created by mac on 07/02/22.
//

import Foundation


enum MessageType
{
    static let SUBSCRIBE_TO_CHANNEL = 11
    static let CONNECT_TO_CHANNEL = 12
    static let UNSUBSCRIBE_DISCONNECT_FROM_CHANNEL = 13
    static let PUBLISH_TO_CHANNEL = 16
    static let SERVER_SUBSCRIBE_TO_CHANNEL = 111
    static let SERVER_UNSUBSCRIBE_DISCONNECT_FROM_CHANNEL = 113
    static let SERVER_PUBLISH_TO_CHANNEL = 116
    static let SERVER_CHANNEL_SENDMSG = 117
    static let LATENCY = 99
    static let SYSTEM_MSG = 0
    static let PARTICIPANT_JOIN = 17
    static let PARTICIPANT_LEFT = 18
    static let CF_CALL_RECEIVED_OR_CALL = 44
    
    static let CF_CALL_RESPONSE = 46
    static let CF_CALL_TIMEOUT = 49
    static let CF_RESPONSE_TRACKER = 48
    static let CF_CALLEE_QUEUE_EXCEEDED = 50
    static let REGISTER_RPC_SERVER = 51
    static let UNREGISTER_RPC_SERVER = 52
    static let CONNECT_TO_RPC_SERVER = 53
    //static let CALL_RPC_FUNCTION = 54
    static let CALL_RPC_FUNCTION_OR_RECEIVED = 54
    static let CALL_CHANNEL_RPC_FUNCTION = 55
    
    static let RPC_CALL_RESPONSE = 56
    static let RPC_CALL_TIMEOUT = 59
    static let RPC_RESPONSE_TRACKER = 58
    static let RPC_CALLEE_QUEUE_EXCEEDED = 60

}
