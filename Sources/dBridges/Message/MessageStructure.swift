//
//  File.swift
//  
//
//  Created by mac on 07/02/22.
//

import Foundation

public class MessageStructure
{
    public let eventname: String = "db"
    public var dbmsgtype: Int
    public var subject: String?
    public var rsub: String?
    public var sid: String?
    public var payload: String?
    
    public var fenceid: String?
    public var rspend: Bool?
    public var rtrack: Bool?
    public var rtrackstat: String?
    public var t1: Int64?
    public var latency: Int64?
    public  var globmatch: Int?
    public var sourceid: String?
    public var sourceip: String?
    public var replylatency: Int?
    public var oqueumonitorid: String?

    init() {
        self.dbmsgtype =  MessageType.SYSTEM_MSG
        self.subject=""
        self.rsub=""
        self.sid=""
        self.payload=nil
        
        self.fenceid=""
        self.rspend=false
        self.rtrack=false
        self.rtrackstat=""
        self.t1=0
        self.latency=0
        self.globmatch=0
        self.sourceid=""
        self.sourceip=""
        self.replylatency=0
        self.oqueumonitorid=""
    }
    
    public func getString(_ inPartmessage: Any? ) -> String? {
        if( (inPartmessage is NSNull) || (inPartmessage == nil)){
            return nil
        }else{
            return (inPartmessage as? String)!
        }
    }
    
    
    public func getArraytoString(_ inPartmessage: Data) -> String?{
        if( (inPartmessage is NSNull) || (inPartmessage == nil)){
            return nil
        }else{
            return String(decoding: inPartmessage , as: UTF8.self)
        }
    }
   
    
    public func getBool(_ inPartmessage: Any? ) -> Bool {
        if( (inPartmessage is NSNull) || (inPartmessage == nil)){
            return false
        }else{
            return (inPartmessage as? Bool)!
        }
    }
    
    public func getInt64(_ inPartmessage: Any? ) -> Int64 {
        if( (inPartmessage is NSNull) || (inPartmessage == nil)){
            return 0
        }else{
            return (inPartmessage as? Int64)!
        }
    }
    
    public func getInt(_ inPartmessage: Any? ) -> Int {
        if( (inPartmessage is NSNull) || (inPartmessage == nil)){
            return 0
        }else{
            return (inPartmessage as? Int)!
        }
    }
    
    
    public  func Parse(_ inmessage: [Any])
    {
        for index in 0...inmessage.count-1{
            switch index {
            case 0:
                self.dbmsgtype =  (inmessage[0] as? Int)!
                break
            case 1:
                self.subject = getString(inmessage[1]) //  (inmessage[1] as? String)!
                break
            case 2:
            
                self.rsub = getString(inmessage[2]) //(inmessage[2] as? String)!
                break
            case 3:
                self.sid =  getString(inmessage[3]) //(inmessage[3] as? String)!
                break
            case 4:
                //let str = String(decoding: data, as: UTF8.self)
                
                //let bytes:[UInt8] = (inmessage[4] as? [UInt8])!
                //print(type(of: inmessage[4]))
                self.payload = getArraytoString( inmessage[4] as! Data)
                break
            case 5:
                self.fenceid = getString(inmessage[5])
                break
            case 6:
                self.rspend = getBool(inmessage[6])
                break
            case 7:
                self.rtrack = getBool(inmessage[7])
                break
            case 8:
                self.rtrackstat =  getString(inmessage[8])
                break
            case 9:
                self.t1 =  getInt64(inmessage[9])
                break
            case 10:
                self.latency = getInt64(inmessage[10])
                break
            case 11:
                self.globmatch = getInt(inmessage[11])
                break
            case 12:
                self.sourceid =  getString(inmessage[12])
                break
            case 13:
                self.sourceip = getString(inmessage[13])
                break
            case 14:
                self.replylatency = getInt(inmessage[14])
                break
            case 15:
                self.oqueumonitorid = getString(inmessage[15])
                break
            default:
           //     print("Structure issue...\(index)")
                break
            }
        }
    }
}
