//
//  File.swift
//  
//
//  Created by mac on 08/02/22.
//

import Foundation


public  class States
{
    public static let  CONNECTED:String = "connected"
    public static let  ERROR:String = "connect_error"
    public static let  DISCONNECTED:String = "disconnected"
    public static let  RECONNECTING:String = "reconnecting"
    public static let  CONNECTING:String = "connecting"
    public static let  STATE_CHANGE:String = "state_change"
    public static let  RECONNECT_ERROR:String = "reconnect_error"
    public static let  RECONNECT_FAILED:String = "reconnect_failed"
    public static let  RECONNECTED:String = "reconnected"
    public static let  CONNECTION_BREAK:String = "connection_break"

    public static let  RTTPONG:String = "rttpong"
    public static let  RTTPING:String = "rttping"

    public static let supportedEvents:[String] = ["connect_error" , "connected",
                                                  "disconnected", "reconnecting",
                                                  "connecting", "state_change",
                                                  "reconnect_error", "reconnect_failed",
                                                  "reconnected", "connection_break",
                                                  "rttpong"];

}
