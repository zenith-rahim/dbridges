//
//  File.swift
//  
//
//  Created by mac on 08/02/22.
//

import Foundation

public class StateChange
{
    private var _previous:String
    private var previous:String {
        get {return _previous}
    }
    
    private var _current:String
    private var current:String {
        get{ return _current}
    }

    public init(_ previous:String ,  _ current:String)
    {
        self._previous = previous
        self._current = current
    }

    
    public  func ToString() -> String
    {
        var m_tempstring:String = ""
        m_tempstring = "{previous:\"" + self._previous + "\" ," +
            "current:\"" + self._current + "\"}"
        return m_tempstring
    }
    
}
