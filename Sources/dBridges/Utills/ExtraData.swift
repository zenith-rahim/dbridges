//
//  File.swift
//  
//
//  Created by mac on 12/04/22.
//

import Foundation

public class ExtraData{
    
    public var  sessionid : String
    public var  libtype: String
    public var  sourceipv4 : String
    public var  sourceipv6 : String
    public var  sysinfo: String
    public var  sourcesysid : String
    public var channelname : String
    
    public init()
    {
        self.sessionid = ""
        self.libtype  = ""
        self.sourceipv4  = ""
        self.sourceipv6  = ""
        self.sysinfo  = ""
        self.sourcesysid  = ""
        self.channelname  = ""
    }
    
    
}
