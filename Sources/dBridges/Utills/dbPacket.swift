//
//  File.swift
//  
//
//  Created by mac on 19/03/22.
//

import Foundation

public class dbPacket {
    var name:String
    var type:String
    var status:String
    var ino:Any
    var count:Int

    init( _ name:String, _ type:String, _ status:String , _ ino: Any){
        self.name = name
        self.type = type
        self.status = status
        self.ino = ino
        self.count = 0
    }
    
}

