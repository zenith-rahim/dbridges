//
//  File.swift
//  
//
//  Created by mac on 09/02/22.
//

import Foundation

public class APIResponce
{
  public var secured:Bool
  public var wsip:String
  public var wsport:String
  public var sessionkey:String
  public var statuscode:Int
  public var reasonphrase:String


   public init()
   {
       self.secured = false
       self.wsip = ""
       self.wsport = ""
       self.sessionkey = ""
       self.statuscode = 0
       self.reasonphrase = ""
   }

   public func update(_ statuscode:Int, _ reasonphrase:String)
   {
       self.statuscode = statuscode
       self.reasonphrase = reasonphrase
   }
    
    public func update(_ json: [String: Any])
    {
        //print("inside update: \(json)")
         let secured = json["secured"] as? Bool
        let wsip = json["wsip"] as? String
        let wsport = json["wsport"] as? String
        let sessionkey = json["sessionkey"] as? String
        let statuscode = 200
        let reasonphrase = ""

        self.secured = secured ?? false
        self.wsip = wsip ?? ""
        self.wsport = wsport ?? ""
        self.sessionkey = sessionkey ?? ""
        self.statuscode = statuscode 
        self.reasonphrase = reasonphrase
        
        //print("status code: \(self.statuscode)")
        
    }
    
    
    public func  ToString()->String
    {
     //var m_tempstring:String = ""
        
     let m_tempstring = "{secured: \"" + String(self.secured) + "\"," +
         "wsip: \"" + self.wsip + "\" ," +
         "wsport: \"" + self.wsport + "\"," +
        "sessionkey: \"" + self.sessionkey + "\" ," +
        "statuscode: \"" + String(self.statuscode) + "\" ," +
        "reasonphrase: \"" + self.reasonphrase +  "\" }"
        
             
     return m_tempstring;
    }

    
}
