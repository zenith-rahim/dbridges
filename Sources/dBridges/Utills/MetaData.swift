//
//  File.swift
//  
//
//  Created by mac on 07/02/22.
//

import Foundation



public class MetaData
{
    
    
    
    private var _channelName:String
    var channelName:String {
        get {return _channelName }
        set {_channelName =  newValue}
    }
    
    private var  _eventName:String
    var  eventName:String {
        get {return _eventName }
        set {_eventName =  newValue }
    }
    
    private var _sourcesysid:String
    var sourcesysid:String {
        get {return _sourcesysid }
        set { _sourcesysid = newValue }
        
    }
    
    
    private var  _seqnum:String
    var  seqnum:String {
        get {return _seqnum}
        set {_seqnum =  newValue}
    }
    
    private var _sessionid:String
    var sessionid:String {
        get {return _sessionid}
        set {_sessionid = newValue}
        
    }
    
    private var _intime:Int64
    public var intime:Int64{
        get{return _intime}
        set{_intime = newValue}
    }
    
    
  public init()
  {
       self._channelName = ""
       self._eventName = ""
       self._sourcesysid = ""
       self._seqnum = ""
       self._sessionid = ""
       self._intime = Int64((Date().timeIntervalSince1970 * 1000).rounded())
 }

    public init(_ channelName:String , _ eventName:String)
    {
        self._channelName = channelName
        self._eventName = eventName
        self._sourcesysid = ""
        self._seqnum = ""
        self._sessionid = ""
        self._intime = Int64((Date().timeIntervalSince1970 * 1000).rounded())
    }
 
    public init(_ channelName:String , _ eventName:String ,  _ sourcesysid:String , _ seqnum:String, _ sessionid:String ,  intime:Int64)
    {
        self._channelName = channelName
        self._eventName = eventName
        self._sourcesysid = sourcesysid
        self._seqnum = seqnum
        self._sessionid = sessionid
        self._intime = intime
    }
    
    
    
    public func ToString() -> String
    {
        var m_tempstring:String = ""
        m_tempstring = "{channelName:\"" +  self._channelName + "\"" +
            ", eventName:\"" +  self._eventName +  "\"" +
            ", sourceid:\"" +  self._sourcesysid +  "\"" +
            ", sqnum:\"" +  self._seqnum +  "\"" +
            ", sessionid:\"" +  self._sessionid +  "\"" +
            ", intime:\"" +  String(self._intime) +  "\"}"
        
        return m_tempstring
    }
    
}
