//
//  File.swift
//  
//
//  Created by mac on 08/02/22.
//

import Foundation

public class Util
{
    
   static func stringTOArray(_ data:String)-> [UInt8] {
        var buffer: [UInt8];
        
        if !data.isEmpty{
            let bytes = data.utf8
             buffer = [UInt8](bytes)
        }else{
            let bytes = "".utf8
            buffer = [UInt8](bytes)
        }
        return buffer
    }

   static func GenerateUniqueId(_ len:Int)->String{
        var number = String()
        for _ in 1...len{
            number += "\(Int.random(in: 1...9))"
        }
        return number
    }

    
   static func updatedBNewtworkSC(_ dbcore: dBridges , _ dbmsgtype: Int , _ channelName: String , _ sid: String, _ channelToken: String, _ subject: String , _ source_id: String, _ t1: Int64, _ seqnum: Int64)->Bool{
        
        
        var tseqnum:String = ""
        tseqnum =  (seqnum == 0) ? "" : String(seqnum)

    
    let  msgDbp:[Any] = [
             dbmsgtype,
            (subject.isEmpty) ? NSNull() : subject ,
            NSNull(),
            sid,
            (channelToken.isEmpty) ? Data("".utf8) : Data( channelToken.utf8),
            channelName,
            NSNull(),
            NSNull(),
            NSNull(),
            (t1 == 0) ? NSNull() :  t1,
            NSNull(),
            NSNull(),
            (source_id.isEmpty) ? NSNull() :  source_id,
            NSNull(),
            NSNull(),
        (tseqnum.isEmpty) ? NSNull() :  tseqnum]
        
            return dbcore.send(msgDbp)
        }

    static func  updatedBNewtworkCF(_ dbcore: dBridges, _ dbmsgtype: Int, _ sessionid: String, _ functionName: String, _ returnSubject: String, _ sid: String, _ payload: String, _ rspend: Bool, _ rtrack: Bool) ->Bool {
        
        let msgDbp:[Any] = [
          "db",
          dbmsgtype,
          functionName,
          returnSubject,
          sid,
          stringTOArray(payload),
          sessionid,
          rspend,
          rtrack,
          "",
          0,
          0,
          0,
          "",
          "",
          0,
          ""]
        return dbcore.send(msgDbp)
      }
    
    
    
}

