//
//  File.swift
//  
//
//  Created by mac on 04/04/22.
//

import Foundation


public class AccessResponse {
    var dbobject: Any   //channels dbchannel;
    var Name: String;
    var sid: String;
    var mtype: Int;

   public init(_ mtype: Int , _ Name: String, _ sid: String ,  _ dbobject: Any) {
        self.dbobject = dbobject;
        self.mtype = mtype;
        self.Name = Name;
        self.sid =  sid;
    }
    
   public func end(_ data:AccessInfo){
       if self.dbobject is Channels{
            let caller = self.dbobject as! Channels
            caller._send_to_dbr(self.mtype ,  self.Name , self.sid , data );
        }
        
       if self.dbobject is Rpc{
           let caller = self.dbobject as! Rpc
            caller._send_to_dbr(self.mtype ,  self.Name , self.sid , data );
    }
        
        
    }

    public func exception(_ info:String){
        let accessinfo:AccessInfo = AccessInfo(9, info , "")
        self.end(accessinfo)
    }
}

