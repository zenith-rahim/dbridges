//
//  File.swift
//  
//
//  Created by mac on 04/04/22.
//

import Foundation


public class AccessInfo: Decodable{
    public var statuscode: Int
    public  var error_message: String
    public var accesskey: String
    
   public init() {
        self.statuscode =  9
        self.error_message = "unknow error"
        self.accesskey = ""
    }
    
   
   public init(_ scode: Int , _ error_message: String, _ accesskey: String) {
        self.statuscode = scode;
        self.error_message = error_message;
        self.accesskey = accesskey;
    }

    
}
