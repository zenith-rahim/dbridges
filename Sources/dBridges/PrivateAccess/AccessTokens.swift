//
//  File.swift
//  
//
//  Created by mac on 04/04/22.
//

import Foundation


enum AccessTokens
{
    
    static let CHANNELSUBSCRIBE = "channel.subscribe"
    static let CHANNELCONNECT = "channel.connect"
    static let RPCCONNECT = "rpc.connect"
    static let RPCREGISTER = "rpc.register"
    static let SYSTEM_CHANNELSUBSCRIBE = "system_channel.subscribe"
    
 
}
