//
//  File.swift
//  
//
//  Created by mac on 08/04/22.
//

import Foundation


enum RpcStatus
{
   static let RPC_CONNECTION_INITIATED = "rpc_connection_initiated"
   static let RPC_CONNECTION_PENDING = "rpc_connection_pending"
   static let RPC_CONNECTION_ACCEPTED = "rpc_connection_accepted"
   static let RPC_CONNECTION_ERROR = "rpc_connection_error"
   
    static let RPC_DISCONNECT_INITIATED = "rpc_disconnect_initiated"
   static let RPC_DISCONNECT_ACCEPTED = "rpc_disconnect_accepted"
   static let RPC_DISCONNECT_ERROR = "rpc_disconnect_error"
}
