//
//  File.swift
//  
//
//  Created by mac on 07/04/22.
//

import Foundation

public class RpcResponse{
    private var type:String
    private var _functionName:String
    private var _returnSubsect:String
    private var _sid: String
    private var _dbcore:dBridges
    private var _isend: Bool
    private var _id:String
    public var tracker: Bool
    
    public init(_ type: String , _ functionName: String, _ returnSubect: String, _ sid: String, _ dbcoreobject: dBridges){
        self.type = type
        self._functionName = functionName
        self._returnSubsect = returnSubect
        self._sid = sid
        self._dbcore = dbcoreobject
        self._isend = false
        self._id = returnSubect
        self.tracker = false
    }
    
    public func id()->String{
        return self._id
    }
    
    public func next(_ data: String) throws {
        let cstatus:Bool
       if (!self._isend) {
        if (self.type ==  "CF"){
            cstatus = Util.updatedBNewtworkCF(self._dbcore, MessageType.CF_CALL_RESPONSE, "", self._returnSubsect, "", self._sid, data, self._isend, self.tracker)
        }else{
            cstatus = Util.updatedBNewtworkCF(self._dbcore, MessageType.RPC_CALL_RESPONSE, "", self._returnSubsect, "", self._sid, data, self._isend, self.tracker)
        }
        
        if(!cstatus){
            if (self.type ==  "CF"){
                throw dBError("E068")
            }else{
                throw dBError("E079")
            }
        }
        
       }else{
        if (self.type ==  "CF"){
            throw dBError("E105")
        }else{
            throw dBError("E106")
        }
       }
        
        
    }
    
    
    public func end(_ data:String) throws {
        let cstatus:Bool
        if (!self._isend) {
            self._isend =  true
            if (self.type ==  "CF"){
                cstatus = Util.updatedBNewtworkCF(self._dbcore, MessageType.CF_CALL_RESPONSE, "", self._returnSubsect, "", self._sid, data, self._isend, self.tracker)
            }else{
                cstatus = Util.updatedBNewtworkCF(self._dbcore, MessageType.RPC_CALL_RESPONSE, "", self._returnSubsect, "", self._sid, data, self._isend, self.tracker)
            }
            
            if(!cstatus){
                if (self.type ==  "CF"){
                    throw dBError("E068")
                }else{
                    throw dBError("E079")
                }
            }
        }else{
            if (self.type ==  "CF"){
                throw dBError("E105")
            }else{
                throw dBError("E106")
            }
        }
    }
    
    public func exception(_ errorCode: String, _ errorMessage: String) throws {
        do {
            let data:[String:String] =  [ "c": errorCode, "m": errorMessage ]
            
            try self.end(data.description)
        }catch {
            throw error
        }
    }
    
    
    
}
