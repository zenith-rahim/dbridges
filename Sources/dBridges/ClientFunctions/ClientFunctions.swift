//
//  File.swift
//  
//
//  Created by mac on 06/04/22.
//

import Foundation


public class ClientFunctions{
    
   // private var local_register: [String: [Any]]
  //  private var global_register: [Any]

    
    private var _dispatch:EventDispatcher
    private var _dbcore:dBridges
    private var enable:Bool
    public var functions:dBClientFunctions?
    private var _functionNames:[String]
        //this._functionNames = ["cf.response.tracker", "cf.callee.queue.exceeded"];
    
    public init(_ dbcore: dBridges)
    {
        self._dispatch = EventDispatcher()
        self._dbcore =  dbcore
        self.enable = false
        self.functions =  nil
        self._functionNames =  ["cf.response.tracker", "cf.callee.queue.exceeded"]
    }
    
    
    public func _verify_function() throws -> Bool {
        var mflag:Bool = false;
        if (self.enable) {
            if(self.functions == nil) {
                throw (dBError("E009"))
            }
            
           // if(!(self.functions is dBClientFunctions)){ throw (dBError("E010")) }
            mflag =  true
        }else{
          mflag = true
        }
        return mflag;
      }
    
    
    
    public func regfn(_ functionName:String, _ callback:  @escaping  dBInFunction) throws {
        if (functionName.isEmpty) { throw dBError("E110") }
        //if( callback == nil)  { throw dBError("E111") }
        if (self._functionNames.contains(functionName)) { throw dBError("E110") }
        try self._dispatch.bind(functionName, callback)
        
    }
    
    
    public func unregfn(_ functionName: String, _ callback: @escaping dBInFunction) {
        if(self._functionNames.contains(functionName)) { return }
        self._dispatch.unbind(functionName, callback);
      }

    
    public func bind(_ functionName:String, _ callback:  @escaping  dBInFunction) throws {
        if (functionName.isEmpty) { throw dBError("E066") }
        //if( callback == nil)  { throw dBError("E067") }
        if (self._functionNames.contains(functionName)) { throw dBError("E066") }
        try self._dispatch.bind(functionName, callback)
        
    }
    
    public func unbind(_ functionName: String, _ callback: @escaping dBInFunction) {
        if(self._functionNames.contains(functionName)) { return }
        self._dispatch.unbind(functionName, callback);
      }

    

    
    public func _handle_dispatcher(_ functionName:String, _ returnSubect: String, _ sid: String, _ payload: String) {
        let response = RpcResponse("CF", functionName, returnSubect, sid, self._dbcore)
        self._dispatch.emit_clientfunction(functionName, payload, response)
      }
    
    
    public func _handle_tracker_dispatcher(_ responseid: String, _ errorcode: String) {

        self._dispatch.emit_clientfunction("cf.response.tracker", responseid, errorcode)
      }

      public func _handle_exceed_dispatcher() {
        let err = dBError("E070");
        err.updateCode("CALLEE_QUEUE_EXCEEDED")
        self._dispatch.emit_clientfunction("cf.callee.queue.exceeded", err, "")
      }

      public func resetqueue() throws {
        let m_status  = Util.updatedBNewtworkCF(self._dbcore, MessageType.CF_CALLEE_QUEUE_EXCEEDED, "", "", "", "", "", false, false)
        if (!m_status) { throw  dBError("E068") }
      }

    
    
    
}
